Source: ring-defaults-clojure
Section: java
Priority: optional
Maintainer: Debian Clojure Maintainers <team+clojure@tracker.debian.org>
Uploaders: Apollon Oikonomopoulos <apoikos@debian.org>
Build-Depends:
 debhelper (>= 10),
 javahelper (>= 0.32),
 maven-repo-helper (>= 1.7),
 clojure (>= 1.8),
 libtext-markdown-perl | markdown,
 libring-core-clojure (>= 1.6.0),
 libring-ssl-clojure (>= 0.3.0),
 libring-headers-clojure (>= 0.3.0),
 libring-anti-forgery-clojure (>= 1.1.0),
 libring-mock-clojure (>= 0.3.0),
 libservlet3.1-java,
 default-jdk-headless
Standards-Version: 4.0.0
Vcs-Git: https://salsa.debian.org/clojure-team/ring-defaults-clojure.git
Vcs-Browser: https://salsa.debian.org/clojure-team/ring-defaults-clojure
Homepage: https://github.com/ring-clojure/ring-defaults

Package: libring-defaults-clojure
Architecture: all
Depends:
 ${java:Depends},
 ${misc:Depends}
Recommends: ${java:Recommends}
Description: Ring middleware that provides sensible defaults
 Knowing what middleware to add to a Ring application, and in what
 order, can be difficult and prone to error.
 .
 This library attempts to automate the process, by providing sensible
 and secure default configurations of Ring middleware for both websites
 and HTTP APIs.
